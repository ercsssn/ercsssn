<h1><img src="https://emojis.slackmojis.com/emojis/images/1531849430/4246/blob-sunglasses.gif?1531849430" width="30"/> Hey! Nice to see you.</h1>

<p>Welcome to my page! </br> I'm John Ericsson, Front-End Web Developer from <img src="https://cdn-icons-png.flaticon.com/512/197/197561.png" width="13"/> <b>Pasay City, Philippines.</b> </p>

### Stuff I code with
<p>
  <a href="https://www.python.org/"> 
    <img alt="Python" src="https://img.shields.io/badge/-Python-000000?style=flat-square&logo=python&logoColor=3776AB" />
  </a>
  <a href="https://numpy.org/">
    <img alt="numpy" src="https://img.shields.io/badge/-NumPy-000000?style=flat-square&logo=numpy&logoColor=013243" />
  </a>
  <a href="https://pandas.pydata.org/">
    <img alt="pandas" src="https://img.shields.io/badge/-pandas-000000?style=flat-square&logo=pandas&logoColor=150458" />
  </a>
  <a href="https://matplotlib.org/">
    <img alt="matplotlib" src="https://img.shields.io/badge/-matplotlib-000000?style=flat-square&logo=python&logoColor=white" />
  </a>
  <a href="https://seaborn.pydata.org/">
    <img alt="seaborn" src="https://img.shields.io/badge/-seaborn-000000?style=flat-square&logo=python&logoColor=white" />
  </a>
  <a href="https://jupyter.org/">
    <img alt="jupyter" src="https://img.shields.io/badge/-Jupyter Notebook-000000?style=flat-square&logo=jupyter&logoColor=F37626" />
  </a>
  <a href="https://www.anaconda.com/">
    <img alt="anaconda" src="https://img.shields.io/badge/-Anaconda-000000?style=flat-square&logo=anaconda&logoColor=44A833" />
  </a>
  
<!--    <h2></h2> -->
</p> 
<p>
  <a href="https://whatwg.org/">
    <img alt="HTML" src="https://img.shields.io/badge/-HTML5-000000?style=flat-square&logo=html5&logoColor=E34F26" />
  </a>
  <a href="https://www.w3.org/TR/CSS/#css">
    <img alt="CSS" src="https://img.shields.io/badge/-CSS3-000000?style=flat-square&logo=css3&logoColor=1572B6" />
  </a>
  <a href="https://www.ecma-international.org/publications-and-standards/standards/ecma-262/">
    <img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-000000?style=flat-square&logo=JavaScript&logoColor=F7DF1E" />
  </a>
  <a href="https://reactjs.org/">
    <img alt="React" src="https://img.shields.io/badge/-React-000000?style=flat-square&logo=react&logoColor=61DAFB" />
  </a>
  <a href="https://expressjs.com/">
    <img alt="Express" src="https://img.shields.io/badge/-Express-000000?style=flat-square&logo=express&logoColor=white" />
  </a>
  <a href="https://nodejs.org/en/">
    <img alt="Node.js" src="https://img.shields.io/badge/-Node.js-000000?style=flat-square&logo=node.js&logoColor=339933" />
  </a>
  <a href="https://www.php.net/">
    <img alt="PHP" src="https://img.shields.io/badge/-PHP-000000?style=flat-square&logo=php&logoColor=777BB4" />
  </a>
  <a href="https://laravel.com/">
    <img alt="Laravel" src="https://img.shields.io/badge/-Laravel-000000?style=flat-square&logo=laravel&logoColor=FF2D20" />
  </a>
<!-- <h2></h2> -->
</p>
<p>
  <a href="https://www.sqlite.org/index.html">
    <img alt="SQLite" src="https://img.shields.io/badge/-SQLite-000000?style=flat-square&logo=sqlite&logoColor=003B57" />
  </a>
  <a href="https://www.mysql.com/">
    <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-000000?style=flat-square&logo=mysql&logoColor=4479A1" />
  </a>
  <a href="https://www.mongodb.com/">
    <img alt="MongoDB" src="https://img.shields.io/badge/-MongoDB-000000?style=flat-square&logo=mongodb&logoColor=47A248" />
  </a>
<!-- <h2></h2> -->
</p>
<h3>Open source projects</h3>
<table>
  <thead align="center">
    <tr border: none;>
      <td><b>Projects</b></td>
      <td><b>Repositories</b></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://ercsssn.github.io/etch-a-sketch-project/"><b>Etch-a-Sketch</b></a></td>
      <td><a href="https://github.com/ercsssn/etch-a-sketch-project"><img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=ercsssn&repo=etch-a-sketch-project&title_color=fff&icon_color=fff&text_color=fff&bg_color=000000&hide_border=true&show_owner=true" /></a></td>
    </tr>
    <tr>
      <td><a href="https://ercsssn.github.io/landing-page-project/"><b>Landing Page</b></a></td>
      <td><a href="https://github.com/ercsssn/landing-page-project"><img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=ercsssn&repo=landing-page-project&title_color=fff&icon_color=fff&text_color=fff&bg_color=000000&hide_border=true&show_owner=true" /></a></td>
    </tr>
    <tr>
      <td><a href="https://ercsssn.github.io/rock-paper-scissors-project/"><b>Rock Paper Scissors</b></a></td>
      <td><a href="https://github.com/ercsssn/rock-paper-scissors-project"><img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=ercsssn&repo=rock-paper-scissors-project&title_color=fff&icon_color=fff&text_color=fff&bg_color=000000&hide_border=true&show_owner=true" /></a></td>
    </tr>
  </tbody>
</table>

### 📊 GitHub Stats

| <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"><img align="center" src="https://github-readme-stats.vercel.app/api?username=ercsssn&show_icons=true&custom_title=ercsssn's Stats&title_color=fff&icon_color=fff&text_color=fff&bg_color=000000&hide_border=true" alt="ercsssn github stats" /></a> | <a href="https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-programming-scripting-and-markup-languages"><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=ercsssn&title_color=fff&text_color=fff&bg_color=000000&hide_border=true&hide=html,css,blade,shell,vue" /></a> |
| ------------- | ------------- |
